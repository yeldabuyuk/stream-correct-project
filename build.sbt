name := "SA_project1"

version := "0.1"

scalaVersion := "2.13.2"
libraryDependencies += "com.typesafe.akka" % "akka-actor_2.13" % "2.5.23"
libraryDependencies += "com.typesafe.akka" % "akka-stream_2.13" % "2.5.23"
libraryDependencies += "com.typesafe.play" %% "play-json" % "2.9.0"
libraryDependencies += "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.12"
libraryDependencies ++= Seq(
  "com.lightbend.akka" %% "akka-stream-alpakka-json-streaming" % "2.0.1",
  "com.typesafe.akka" %% "akka-stream" % "2.6.8"
)
libraryDependencies ++= Seq(
  "edu.stanford.nlp" % "stanford-corenlp" % "3.6.0",
  "edu.stanford.nlp" % "stanford-corenlp" % "3.6.0" classifier "models"
)
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.1.3" % Runtime
