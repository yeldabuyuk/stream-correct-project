
import java.nio.file.Paths

import akka.{Done, NotUsed}
import akka.actor.ActorSystem
import akka.stream.scaladsl.{Broadcast, Compression, FileIO, Flow, GraphDSL, JsonFraming, Keep, Merge, RunnableGraph, Sink, Source}
import akka.stream.{ActorMaterializer, ClosedShape, IOResult, Materializer, OverflowStrategy}
import akka.util.ByteString
import play.api.libs.json._

import scala.concurrent.duration.Duration
import scala.concurrent.{ExecutionContext, Future}

/**
 * Main code.
 */
object TweetStream extends App {

  implicit val system: ActorSystem = ActorSystem("Twitter")
  implicit val materializer: Materializer = ActorMaterializer()
  implicit val executionContext: ExecutionContext = materializer.executionContext

  // define path to file.
  val path = Paths.get("src/main/assign1.json2.gz")
  val fileSource: Source[ByteString, Future[IOResult]] = FileIO.fromPath(path)

  // result file
  val sentimentsFile = Paths.get("tweets_sentiments_en.txt")

  // counters
  val quotedCounter = new TweetCounter
  val repliesCounter = new TweetCounter
  val retweetedCounter = new TweetCounter
  val normalCounter = new TweetCounter

  //combiner
  val statisticsGenerator = new CombineStatistics

  //flows
  val uncompress: Flow[ByteString, ByteString, NotUsed] = Flow[ByteString].via(Compression.gunzip()).buffer(50, OverflowStrategy.backpressure)
  val mapToTweet: Flow[ByteString, JsResult[Tweet], NotUsed] = JsonFraming.objectScanner(99999).map(_.utf8String).map(x => Json.parse(x)).map(x => Json.fromJson[Tweet](x))

  // filter flows
  val filterEnglish: Flow[JsResult[Tweet], JsResult[Tweet], NotUsed] = Flow[JsResult[Tweet]].filter(t => t.get.lang == Some("en"))
  val filterRetweeted: Flow[JsResult[Tweet], JsResult[Tweet], NotUsed] = Flow[JsResult[Tweet]].filter(t => t.get.retweeted_status != None)
  val filterReplies: Flow[JsResult[Tweet], JsResult[Tweet], NotUsed] = Flow[JsResult[Tweet]].filter(t => t.get.in_reply_to_screen_name != None)
  val filterQuoted: Flow[JsResult[Tweet], JsResult[Tweet], NotUsed] = Flow[JsResult[Tweet]].filter(t => t.get.quoted_status != None)
  val filterNormal: Flow[JsResult[Tweet], JsResult[Tweet], NotUsed] = Flow[JsResult[Tweet]].filter(t => (t.get.retweeted_status == None && t.get.in_reply_to_screen_name == None && t.get.quoted_status == None))

  val combineStatistics: Flow[JsResult[Tweet], JsResult[Tweet], NotUsed] = Flow[JsResult[Tweet]].map(t => statisticsGenerator.setStatistics(t,quotedCounter.amount_tweets, repliesCounter.amount_tweets, retweetedCounter.amount_tweets, normalCounter.amount_tweets))

  // count flows
  val countRetweets: Flow[JsResult[Tweet], JsResult[Tweet], NotUsed] = Flow[JsResult[Tweet]].map(t => retweetedCounter.incrementCount(t))
  val countReplies: Flow[JsResult[Tweet], JsResult[Tweet], NotUsed] = Flow[JsResult[Tweet]].map(t => repliesCounter.incrementCount(t))
  val countNormal: Flow[JsResult[Tweet], JsResult[Tweet], NotUsed] = Flow[JsResult[Tweet]].map(t => normalCounter.incrementCount(t))
  val countQuoted: Flow[JsResult[Tweet], JsResult[Tweet], NotUsed] = Flow[JsResult[Tweet]].map(t => quotedCounter.incrementCount(t))

  //sentiment flows
  val analyze: Flow[JsResult[Tweet], List[String], NotUsed] = Flow[JsResult[Tweet]].throttle(1, Duration(1, "seconds")).map(t => SentimentAnalyzer.mainSentiment(t, t.get.full_text))

  //sinks
  val statisticSink: Sink[JsResult[Tweet], Future[Done]] = Sink.foreach[JsResult[Tweet]](t => statisticsGenerator.printStatistics())
  val sentimentsink: Sink[List[String], Future[IOResult]] = Flow[List[String]].map(el => ByteString(el + "\n")).toMat(FileIO.toPath(sentimentsFile))(Keep.right)

  //DSL graph
  val g = RunnableGraph.fromGraph(GraphDSL.create() { implicit builder: GraphDSL.Builder[NotUsed] =>
    import GraphDSL.Implicits._

    val bcast = builder.add(Broadcast[JsResult[Tweet]](5))
    val merge = builder.add(Merge[JsResult[Tweet]](4))

    fileSource ~> uncompress ~> mapToTweet ~> filterEnglish  ~> bcast ~> filterNormal ~> countNormal ~> merge  ~> combineStatistics ~>  statisticSink
                                                                bcast  ~> filterRetweeted  ~> countRetweets ~> merge
                                                                bcast ~> filterQuoted  ~> countQuoted ~> merge
                                                                bcast ~> filterReplies  ~> countReplies ~> merge
    //sentiment
    bcast ~>  filterNormal ~> analyze ~> sentimentsink
    ClosedShape
  })
  g.run()

}