import play.api.libs.json.JsResult

/**
 * Takes care of the calculations and formats the statistics.
 */
class CombineStatistics() {

  // Initial values.
  var totalTweets = 0
  var quotedTweets = 0
  var repliesTweets = 0
  var retweetedTweets = 0
  var normalTweets = 0

  /**
   * Updates the counts.
   * @param tweet
   * @param quotes
   * @param replies
   * @param retweets
   * @param normal
   * @return
   */
  def setStatistics(tweet: JsResult[Tweet], quotes: Int, replies: Int, retweets: Int, normal: Int): JsResult[Tweet] = {
    totalTweets = quotes + replies + retweets + normal
    quotedTweets = quotes
    repliesTweets = replies
    retweetedTweets = retweets
    normalTweets = normal
    tweet
  }

  /**
   * Given an amount of a category tweet and the total tweets, we calculate the percentage.
   * @param tweets
   * @param total
   * @return
   */
  def calculatePercentage(tweets: Int, total: Int): Double ={
    BigDecimal((tweets.toFloat/total.toFloat) * 100).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble
  }

  /**
   * Method to print the statistics in a formatted manner.
   */
  def printStatistics(): Unit = {
    if (totalTweets % 20 == 0){
      println(totalTweets + " Tweets received. Counts & percentage by type:")
      println("-> Normal Tweets: " + normalTweets + " (" + calculatePercentage(normalTweets,totalTweets) + "%)")
      println("-> Retweets: " + retweetedTweets + " (" + calculatePercentage(retweetedTweets,totalTweets) + "%)")
      println("-> Quoted Tweets: " + quotedTweets + " (" + calculatePercentage(quotedTweets,totalTweets) + "%)")
      println("-> Reply Tweets: " + repliesTweets + " (" + calculatePercentage(repliesTweets,totalTweets) + "%)")
    }
  }
}
