import play.api.libs.json.JsResult

class TweetCounter(){
  var amount_tweets = 0;

  def incrementCount(tweet: JsResult[Tweet]): JsResult[Tweet] ={
    amount_tweets = amount_tweets + 1
    tweet
  }
}