
import edu.stanford.nlp.pipeline.{Annotation, StanfordCoreNLP}
import java.util.Properties

import edu.stanford.nlp.ling.CoreAnnotations
import edu.stanford.nlp.neural.rnn.RNNCoreAnnotations
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations.SentimentAnnotatedTree

import scala.jdk.CollectionConverters._
import play.api.libs.json.JsResult

/**
 * Code based on several sources:
 * https://github.com/harpribot/corenlp-scala-examples/blob/master/src/main/scala/com/harpribot/corenlp_scala/SentimentAnalysisExample.scala
 * https://shekhargulati.com/2017/09/30/sentiment-analysis-in-scala-with-stanford-corenlp-2/
 */

object SentimentAnalyzer {

  def toSentiment(sentiment: Int): String = sentiment match {
    case x if x == 0 || x == 1 => "Negative"
    case 2 => "Neutral"
    case x if x == 3 || x == 4 => "Positive"
  }
  val props = new Properties()
  props.setProperty("annotators", "tokenize, ssplit, parse, sentiment")
  val pipeline: StanfordCoreNLP = new StanfordCoreNLP(props)

  def mainSentiment(tweet: JsResult[Tweet], input: Option[String]): List[String] = input match {
    case Some(string: String) if !string.isEmpty => extractSentiment(input.get)
    case _ => throw new IllegalArgumentException("input can't be null or empty")
  }

  def extractSentiment(input: String): List[String] ={
    val annotation = pipeline.process(input)
    val sentences = annotation.get(classOf[CoreAnnotations.SentencesAnnotation]).asScala.toList

    sentences
      .map(sentence => (sentence, sentence.get(classOf[SentimentAnnotatedTree])))
      .map { case (sentence, tree) => ( toSentiment(RNNCoreAnnotations.getPredictedClass(tree)) , sentence.toString) }
      .map(x => x.toString())

  }

}