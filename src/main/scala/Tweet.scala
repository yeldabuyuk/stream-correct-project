import play.api.libs.json.{Format, JsPath, JsResult, JsValue, Json, Reads}

/**
 * Case class to parse the Json to a Tweet class. Not all of the parameters are parsed, only those that we use.
 * @param quoted_status
 * @param in_reply_to_screen_name
 * @param lang
 * @param retweeted_status
 * @param full_text
 */
case class Tweet(quoted_status: Option[Tweet], in_reply_to_screen_name: Option[String], lang: Option[String], retweeted_status: Option[Tweet], full_text: Option[String])

object Tweet {
  implicit val modelFormat = Json.reads[Tweet]
}

